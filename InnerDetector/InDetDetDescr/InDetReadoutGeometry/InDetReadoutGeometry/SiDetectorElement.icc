/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file SiDetectorElement.icc
 **/

namespace InDetDD {

  ///////////////////////////////////////////////////////////////////
  // Inline methods:
  ///////////////////////////////////////////////////////////////////

  inline void SiDetectorElement::setNextInEta(const SiDetectorElement* element)
  {
    m_nextInEta = element;
  }

  inline void SiDetectorElement::setPrevInEta(const SiDetectorElement* element)
  {
    m_prevInEta = element;
  }

  inline void SiDetectorElement::setNextInPhi(const SiDetectorElement* element)
  {
    m_nextInPhi = element;
  }

  inline void SiDetectorElement::setPrevInPhi(const SiDetectorElement* element)
  {
    m_prevInPhi = element;
  }

  inline void SiDetectorElement::setOtherSide(const SiDetectorElement* element) // For SCT only
  {
    m_otherSide = element;
  }

  inline const SiDetectorElement * SiDetectorElement::nextInEta() const
  {
    return m_nextInEta;
  }
    
  inline const SiDetectorElement * SiDetectorElement::prevInEta() const
  {
    return m_prevInEta;
  }
    
  inline const SiDetectorElement * SiDetectorElement::nextInPhi() const
  {
    return m_nextInPhi;
  }
    
  inline const SiDetectorElement * SiDetectorElement::prevInPhi() const
  {
    return m_prevInPhi;
  }
    
  inline const SiDetectorElement * SiDetectorElement::otherSide() const
  {
    return m_otherSide;
  }

  inline bool SiDetectorElement::isEndcap() const
  {
    return (!isBarrel() && !isDBM());
  }
    
  inline const SiDetectorDesign& SiDetectorElement::design() const
  {
    return *m_design;
  }
    
  inline double SiDetectorElement::phiPitch() const
  {
    return m_design->phiPitch();
  }
    
  inline double SiDetectorElement::phiPitch(const Amg::Vector2D& localPosition) const
  {
    return m_design->phiPitch(localPosition);
  }
    
  inline InDetDD::CarrierType SiDetectorElement::carrierType() const
  {
    return m_design->carrierType();
  }

  inline bool SiDetectorElement::swapPhiReadoutDirection() const
  {
    if (m_firstTime) {
      std::lock_guard<std::mutex> lock(m_mutex);
      if (m_firstTime) updateCache(); // In order to set m_phiDirection
    }
    // equivalent to (m_design->swapHitPhiReadoutDirection() xor !m_phiDirection)
    return ((!m_design->swapHitPhiReadoutDirection() && !m_phiDirection)
          || (m_design->swapHitPhiReadoutDirection() &&  m_phiDirection));
  }
    
  inline bool SiDetectorElement::swapEtaReadoutDirection() const
  {
    if (m_firstTime) {
      std::lock_guard<std::mutex> lock(m_mutex);
      if (m_firstTime) updateCache(); // In order to set m_etaDirection
    }
    // equivalent to (m_design->swapHitEtaReadoutDirection() xor !m_etaDirection)
    return ((!m_design->swapHitEtaReadoutDirection() && !m_etaDirection)
          || (m_design->swapHitEtaReadoutDirection() &&  m_etaDirection));
  }
    
  inline SiCellId SiDetectorElement::gangedCell(const SiCellId& cellId) const
  {
    return  m_design->gangedCell(cellId);
  }
    
  inline MsgStream& SiDetectorElement::msg(MSG::Level lvl) const
  {
    return m_commonItems->msg(lvl);
  }

  inline bool SiDetectorElement::msgLvl(MSG::Level lvl) const
  {
    return m_commonItems->msgLvl(lvl);
  }



} // namespace InDetDD
